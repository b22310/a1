<?php require_once './code.php';?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1</title>
</head>
<body>
	<h3>2. Create a function named getFullAddress() that will take four arguments:</h3>

	<p><?php echo getFullAddress('Philippines', 'Antipolo', 'Rizal', 'Bagong Nayon');?></p>

	<h3>3. Have this function return the concatenated arguments to result in a coherent address.</h3>

	<p><?php echo $address; ?></p>


	<h3>4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
	- A+ (98 to 100)
	- A (95 to 97)
	- A- (92 to 94)
	- B+ (89 to 91)
	- B (86 to 88)
	- B- (83 to 85)
	- C+ (80 to 82)
	- C (77 to 79)
	- C- (75 to 76)
	- D (75 below)</h3>

	<p>75: <?php echo getLetterGrade(75);?></p>

</body>
</html>